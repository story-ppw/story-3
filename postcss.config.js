module.exports = {
	plugins: [
		require('tailwindcss'),
		require('@fullhuman/postcss-purgecss')({
			content: [
				'./app/templates/*.html',
			],
			defaultExtractor: content => content.match(/[A-za-z0-9-_:/]+/g) || []
		})
	]
}
